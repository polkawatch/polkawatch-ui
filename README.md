## Polkawtach

The purpose of this project is to provide analytics of the Polkadot project mainly related to its degree of de-centralization.

## Technology Stack

The project is build on top of the following technologies:

- [Nodejs](https://nodejs.org/en/) v14, as Javascript Runtime Engine
- [React](https://reactjs.org/) as Library forbuilding user interfaces.
- [Gatsby](https://www.gatsbyjs.com/) as Static Application Generator.
- [Chakra](https://chakra-ui.com/) as User Interface Components Library
- [Subquery](https://www.subquery.network/) as Polkadot Query Technology

## Getting started

- install Node 14, via nvm
- install gatsby
- run ```yarn install```
- run ```gatsby develop```

## Datapack update

Polkadot chain analytics are very hard to create, however as they are high level statistics, they are very compact. 
At the time of writing the "data pack" is less than 1kb of compressed data per era.

If the datapack is produced per era in an inmutable way, the DAPP could consume a "Decentralized Data Pack" that it is
produced/updated by the subquery companion independently.

### Manual Datapack processing

For the time being the Distributed DataPack is not inmutable, and contains all the dataset, it is manually updated like this:

- start the polkawatch subquery companion
- run gulp
- run the data pack npm script
- pin on IPFS
- update DATAPACK_CID in gitlabci. 