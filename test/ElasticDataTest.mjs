import {describe, it} from "mocha";
import "it-fails";

import chai from "chai";
const {assert, expect}=chai;

import {Client as Elastic} from "@elastic/elasticsearch";

import rewardsByValidatorRegionQuery from "./KibanaQueries.mjs"

import {
  aboutDataRewards,
  aboutDataEras
} from "../datasrc/index.js"

const elastic = new Elastic({ node: 'http://localhost:9200' })
const index = "pw_reward";

describe("Will test Elastic as DataPack source", function(){

  it.fails("Will test the elastic client is working", async function(){
    const result = await elastic.search({
      index: index,
      body: {
        query: {
          match: { hello: 'world' }
        }
      }
    })
  });

  it.fails("Will test a query exported from kibana", async function(){
    const result = await elastic.search({
      index: index,
      track_total_hits: true,
      body: rewardsByValidatorRegionQuery
    });

    const data=result.body.aggregations[0].buckets.map(b => ({
      value: b["1"].value,
      key: b.key
    }));

    console.log(JSON.stringify(data));

  });

  it.fails("Will test a second query exported from kibana", async function(){
    const result = await elastic.search({
      index: index,
      body: aboutDataEras
    });
    const data=result.body.aggregations[0]
    assert(data.value>10)
  });

  it.fails("Will test a count query exported from kibana", async function(){
    const result = await elastic.search({
      index: index,
      track_total_hits: true,
      body: aboutDataRewards
    });
    const data=result.body.hits.total
    console.log(JSON.stringify(data));
    assert(data.value>10)

  });

});