import {describe, it} from "mocha";
import "it-fails";

import chai from "chai";
const {assert, expect}=chai;

// This Test File is just a cheat sheet on how to use the testing libraries
// Add tests for your favorite test patterns

describe("Mocha Tests", function(){
  it("Will test chai assertions", function(){
    assert(1==1);
    assert(true);
    assert.equal(2,1+1);
    assert.typeOf({},'object');
  });

  it("Will test chai expect api", function(){
    expect('hola').to.be.a('string');
    expect(1+1).to.equal(2);
  });

  it("Will test sync callbacks", function(done){
    setTimeout(function(){
      // timeout runned out!
      done();
    },1);
  });

  // there is a test that fails, perhaps because it is not yet
  // implemented
  it.fails("Will test a not yet implemented test", function(){
    assert(false==true);
  });

  // TODO: Promise sample

  // TODO: Async function sample

});