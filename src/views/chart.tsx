import React from "react";

import {
  Box,
  Container,
  SimpleGrid,
  Flex,
  Heading,
  Text,
  Stack,
} from '@chakra-ui/react';


import { ReactElement } from 'react';

interface FeatureProps {
  text: string;
  iconBg: string;
  icon?: ReactElement;
}

const Feature = ({ text, icon, iconBg }: FeatureProps) => {
  return (
    <Stack direction={'row'} align={'center'}>
      <Flex
        w={8}
        h={8}
        align={'center'}
        justify={'center'}
        rounded={'full'}
        bg={iconBg}>
        {icon}
      </Flex>
      <Text fontWeight={600}>{text}</Text>
    </Stack>
  );
};

export function ChartSection({title, description, chart, legend, h="400px", w="400px"}) {
  return (
    <Container>
      <Stack
        as={Box}
        textAlign={'center'}
        spacing={{ base: 8, md: 8 }}
        py={{ base: 8, md: 8 }}>
        <Heading>{title}</Heading>
        <Text color={'gray.500'} fontSize={'lg'}>
          {description}
        </Text>
      </Stack>
      <SimpleGrid columns={{ base: 1, md: 2 }} spacing={10}>
        <Stack spacing={4}>
          {legend}
        </Stack>
        <Box h={h} w={w}>
          {chart}
        </Box>
      </SimpleGrid>
    </Container>
  );
}
