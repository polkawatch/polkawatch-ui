import React from "react";

import {
  Table,
  TableCaption,
  Thead,
  Tr,
  Th,
  Td,
  Tbody,
} from '@chakra-ui/react';

export  function ValidatorTable({ data, caption, column_names }) {

  return (

    <Table variant="striped" colorScheme="gray">
      <TableCaption>{caption}</TableCaption>
      <Thead>
        <Tr>
          <Th>{column_names[0]}</Th>
          <Th isNumeric>{column_names[1]}</Th>
          <Th isNumeric>{column_names[2]}</Th>
        </Tr>
      </Thead>
      <Tbody>

      {data.map((entry, index) => (
        <Tr>
          <Td>{entry.key}</Td>
          <Td isNumeric>{entry.dot_reward.toLocaleString()}</Td>
          <Td isNumeric>{entry.median_nomination.toLocaleString()}</Td>
        </Tr>
      ))}
      </Tbody>
    </Table>)
}

