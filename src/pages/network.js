import * as React from "react"

import {ChartSection} from "../views";

import {DataPieChart} from "../components/datapiechart"
import {DataStackedChart} from "../components/datastakedchart";
import {ChartLegend} from "../components/chartlegend";

import {FaNetworkWired} from 'react-icons/fa';


import dataNetRewardDistribution from "../data/pack/net-reward-distribution.yaml"
import dataNetRewardEvolution from "../data/pack/net-reward-evolution.yaml"

import Layout from "../components/layout"
import Seo from "../components/seo"
import dataGeoRewardEvolution from "../data/pack/geo-reward-evolution.yaml"
import dataGeoRewardDistribution from "../data/pack/geo-reward-distribution.yaml"

const SecondPage = () => {

  return (
    <Layout>
      <Seo title="Network"/>
      <ChartSection
        title={"Network Distribution"}
        description={"Staking rewards by Network provider of the validator"}
        chart={<DataPieChart data={dataNetRewardDistribution}/>}
        legend={<ChartLegend data={dataNetRewardDistribution} icon={FaNetworkWired}/>}
      />

      <ChartSection
        title={"Network Distribution Evolution"}
        description={"Evolution of staking rewards by network provider of the validator"}
        chart={<DataStackedChart data={dataNetRewardEvolution}/>}
        legend={<ChartLegend data={dataNetRewardDistribution} icon={FaNetworkWired}/>}
      />

    </Layout>
  )
}

export default SecondPage
