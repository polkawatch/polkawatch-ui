import React from "react"

import Layout from "../components/layout"


import {Hero, StatsSection , FeaturesSection } from "../views"

import { BiTimeFive, BiMoney,  BiTrophy, BiMap, BiLock} from 'react-icons/bi';

import {FaNetworkWired} from 'react-icons/fa';

import dataAboutDataDotRewards from "../data/pack/about-data-dot-rewards.yaml"
import dataAboutDataEras from "../data/pack/about-data-eras.yaml"
import dataAboutDataRewards from "../data/pack/about-data-rewards.yaml"

function IndexPage() {
  return (
    <Layout>
      <Hero
        titleStart={"Polkadot decentralization"}
        titleEnd={"Analytics"}
        description={"How decentralized is our blockchain? We analyse the polkadot blockchain and provide the analytics the community needs to make staking decisions that benefit us all."}
      />
      <StatsSection
        title={"About our Dataset"}
        stats={[
          {
            title: "Eras",
            stat: dataAboutDataEras.value,
            icon: BiTimeFive
          },
          {
            title: "Reward Events",
            stat: dataAboutDataRewards.value.toLocaleString(),
            icon: BiTrophy
          },
          {
            title: "DOTs Rewarded",
            stat: Math.round(dataAboutDataDotRewards.value).toLocaleString(),
            icon: BiMoney
          }
        ]}
      />
      <FeaturesSection
        title={"Statistics"}
        description={"Decentralization analytics by:"}
        features={[
          {
            title: "Geography",
            description: "Regards distributed by geography of validation",
            icon: BiMap,
            href: "/geography"
          },
          {
            title: "Network",
            description: "Rewards distributed by network operator of the validator",
            icon: FaNetworkWired,
            href: "/network"
          },
          {
            title: "Validator",
            description: "Rewards distributed by validator group",
            icon: BiLock,
            href: "/validators"

          },
          {
            title: "Nominator",
            description: "Distribution of nominations by value",
            icon: BiMoney,
            href: "/nomination"
          }

        ]}
      />
    </Layout>
  )
}

export default IndexPage



