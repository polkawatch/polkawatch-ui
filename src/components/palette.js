

const COLOR_LINES = ["400", "600", "900"];

const COLORS = ["teal", "blue", "purple", "pink", "green", "cyan"]

const PALETTE = [];

COLOR_LINES.forEach(i => COLORS.forEach(c => PALETTE.push("var(--chakra-colors-"+c+"-"+i+")")));


export function Palette(){
  return PALETTE;
}

export function ColorForItem(i){

  return PALETTE[i%PALETTE.length];

}


